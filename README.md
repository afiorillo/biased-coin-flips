# Biased Coin Flips

**If flipping a biased coin multiple times (e.g. "best two out of three"), does the expectation value tend towards fair, towards the coin's bias, or something else?**

## Setup

Assuming Python3.9 is already installed:

```python
pip install -r requirements.txt
```

## Running

```bash
$ jupyter notebook
```